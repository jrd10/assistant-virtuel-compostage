# Assistant Virtuel Compostage

**Répondre à toutes vos questions à propos du tri à la source de vos biodéchets et le compostage, quelque que soit le lieu et la situation à considérer, le moment où la question est posée et le moyen de communication, écrit et oral, à considérer, à l'aide d'outils personnels connectés ou de bornes interactives**

## État du projet au 1 juin 2024

M. Bardr Laadji vient d'attirer mon attention sur l'outil open source Flowise et toute une stack no-code en ligne. Je pense que je vais basculer le projet vers cet écosystème. 
Présentation du sujet ici : https://youtu.be/Xs4JmE2VVug

## État du projet au 23 mai 2024

Le projet est, à ce stade, en phase de réflexion.
Si vous êtes intéressé par l'initiative, en tant que futur contributeur, utilisateur ou prescripteur, n'hésitez pas à me contacter ici.

Pour ceux qui veulent rentrer directemment dans le sujet, un (très) petit démonstrateur sur Botpress :

**https://mediafiles.botpress.cloud/18a8ff79-6216-498a-a438-83b2a32c31a8/webchat/bot.html**

_Sur la plateforme open source Botpress.com. Version gratuite avec un accès mensuel limité du nombre de questions._

Note importante. Mon sujet ici n'est pas l'intelligence articielle. C'est bien le tri à la source des biodéchets. Je suis à la recherche d'outils clé-en-main de type no-code, incluant quelques paramétrages en code, comme les plateforme de « chatbots ».

_English version soon_


## Pourquoi ce projet ?

Ce projet est né **dans la mouvance du tri à la source des biodéchets** (de table et de jardin).

Membre de Zéro Déchet Troyes, je suis guide composteur, avec une expérience de référent d'un composteur partagé sur ma commune, Saint-André-les-Vergers (Aube, Grand Est).

Je milite pour un **large déploiement du tri à la source des biodéchets par mes concitoyennes et par mes concitoyens** et j'ai pu noté que la PG Prox (le compostage de proximité) générait beaucoup d'interrogation comme :

- Où trouver un composteur ?
- Comment faire pour avoir un composteur chez moi ? J'haite dans cette commune ?
- Que peut-on mettre dans le composteur ?
- Que ne doit-on pas mettre ?
- On m'a dit que l'on pouvait mettre cela et vous me dites que l'on ne peut ???
- Sorry, I don't speak/read French, could you explain me in English how to do? In my country, I use a composter :)
- J'habite en collectif, est-ce que je peux faire du compostage ?
- J'habite en copropriété, est-ce que je peux faire du compostage ?
- Et pour mon restaurant, comment je fais ???

Et bien d'autres questions. Comme celle liée au retour à la terre de la matière organique.

Répondre à ces questions, c'est ce que se propose de faire les collectivités territoriales et les organisations qui gravitent autour de ce thème, n'est pas simple. Différentes situations existent. 

La question est à prendre en compte selon :
- La perspective du demandeur (habitat, objectifs...)
- Le processus de compostage à considérer (si connu au départ)
- La politique de l'intercommunalité, et autres autorités, à considérer
- Le niveau de maturité du déploiement de la PG Prox dans le périmètre à considérer
- Autres questions....

Comme chaque cas est un cas particulier, seul un assistant virtuel (basé sur de l'intelligence artificiel) peut relever ce défi. Cet assistant virtuel devra aussi être alimenté par des référentiels bien spécifiques. 

L'interface utilisateur devra tenir de ces différentes situations dans son dialogue avec l'utilisateur.

De plus, le moment où l'utilisateur a besoin de la réponse est aussi à prendre en compte.

Que cela soit à tout moment, à proximité d'un outil en ligne ou sur le lieu du composteur, avec une borne interactive.

De plus, l'assistant virtuel pourra prendre en compte la langue de l'utilisateur et prendra en compte les règles d'accessibilité.


## La vision du projet

À la base, il n'y a pas de protection intellectuelle (je suis issu du monde du libre et open source).

Le présent projet peut donc être copié, et surtout amélioré, sans même citer la source :).


Je préconise un double développement :

- Un développement « Communauté », avec le partage des expériences et des réalisations,

- Un développement « Entreprise », ou « Premium » avec de nouveaux services et produits qui sont propres à l'entité, sans obligation de partage avec la communauté.

Les deux versions pouvant faire l'objet de rénumération.


Le projet exposé ici est dans le domaine public.


## Licence

## Historique du projet

- Le présent projet a fait l'objet d'échanges à l'intérieur de Zéro Déchet Troyes (ZD3) et de Zéro Waste France. Notamment pour une information sur l'utilisation du site https://biodéchets.fr pour le démonstrateur 

- Le projet a été présenté par ZD3 à l'UTT Crunch Time 2024, équipe 412. Se renseigner auprès des organisateurs : https://crunch.utt.fr/ 

## Suite à donner.

Sur le cours terme :

- Partager, communiquer sur le projet,

- Travailler sur le démonstrateur avec botpress

À moyen terme, envisager un déploiement selon la vision du projet.







# Fin du document







----------------------------------------------------------------------------------------------------------------------------

## Add your files

- [ ] [Create](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/jrd10/assistant-virtuel-compostage.git
git branch -M main
git push -uf origin main
```



----

À suivre.


## Integrate with your tools

- [ ] [Set up project integrations](https://gitlab.com/jrd10/assistant-virtuel-compostage/-/settings/integrations)

## Collaborate with your team

- [ ] [Invite team members and collaborators](https://docs.gitlab.com/ee/user/project/members/)
- [ ] [Create a new merge request](https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html)
- [ ] [Automatically close issues from merge requests](https://docs.gitlab.com/ee/user/project/issues/managing_issues.html#closing-issues-automatically)
- [ ] [Enable merge request approvals](https://docs.gitlab.com/ee/user/project/merge_requests/approvals/)
- [ ] [Set auto-merge](https://docs.gitlab.com/ee/user/project/merge_requests/merge_when_pipeline_succeeds.html)

## Test and Deploy

Use the built-in continuous integration in GitLab.

- [ ] [Get started with GitLab CI/CD](https://docs.gitlab.com/ee/ci/quick_start/index.html)
- [ ] [Analyze your code for known vulnerabilities with Static Application Security Testing (SAST)](https://docs.gitlab.com/ee/user/application_security/sast/)
- [ ] [Deploy to Kubernetes, Amazon EC2, or Amazon ECS using Auto Deploy](https://docs.gitlab.com/ee/topics/autodevops/requirements.html)
- [ ] [Use pull-based deployments for improved Kubernetes management](https://docs.gitlab.com/ee/user/clusters/agent/)
- [ ] [Set up protected environments](https://docs.gitlab.com/ee/ci/environments/protected_environments.html)

***

# Editing this README

When you're ready to make this README your own, just edit this file and use the handy template below (or feel free to structure it however you want - this is just a starting point!). Thanks to [makeareadme.com](https://www.makeareadme.com/) for this template.

## Suggestions for a good README

Every project is different, so consider which of these sections apply to yours. The sections used in the template are suggestions for most open source projects. Also keep in mind that while a README can be too long and detailed, too long is better than too short. If you think your README is too long, consider utilizing another form of documentation rather than cutting out information.

## Name
Choose a self-explaining name for your project.

## Description
Let people know what your project can do specifically. Provide context and add a link to any reference visitors might be unfamiliar with. A list of Features or a Background subsection can also be added here. If there are alternatives to your project, this is a good place to list differentiating factors.

## Badges
On some READMEs, you may see small images that convey metadata, such as whether or not all the tests are passing for the project. You can use Shields to add some to your README. Many services also have instructions for adding a badge.

## Visuals
Depending on what you are making, it can be a good idea to include screenshots or even a video (you'll frequently see GIFs rather than actual videos). Tools like ttygif can help, but check out Asciinema for a more sophisticated method.

## Installation
Within a particular ecosystem, there may be a common way of installing things, such as using Yarn, NuGet, or Homebrew. However, consider the possibility that whoever is reading your README is a novice and would like more guidance. Listing specific steps helps remove ambiguity and gets people to using your project as quickly as possible. If it only runs in a specific context like a particular programming language version or operating system or has dependencies that have to be installed manually, also add a Requirements subsection.

## Usage
Use examples liberally, and show the expected output if you can. It's helpful to have inline the smallest example of usage that you can demonstrate, while providing links to more sophisticated examples if they are too long to reasonably include in the README.

## Support
Tell people where they can go to for help. It can be any combination of an issue tracker, a chat room, an email address, etc.

## Roadmap
If you have ideas for releases in the future, it is a good idea to list them in the README.

## Contributing
State if you are open to contributions and what your requirements are for accepting them.

For people who want to make changes to your project, it's helpful to have some documentation on how to get started. Perhaps there is a script that they should run or some environment variables that they need to set. Make these steps explicit. These instructions could also be useful to your future self.

You can also document commands to lint the code or run tests. These steps help to ensure high code quality and reduce the likelihood that the changes inadvertently break something. Having instructions for running tests is especially helpful if it requires external setup, such as starting a Selenium server for testing in a browser.

## Authors and acknowledgment
Show your appreciation to those who have contributed to the project.

## License
For open source projects, say how it is licensed.

## Project status
If you have run out of energy or time for your project, put a note at the top of the README saying that development has slowed down or stopped completely. Someone may choose to fork your project or volunteer to step in as a maintainer or owner, allowing your project to keep going. You can also make an explicit request for maintainers.
